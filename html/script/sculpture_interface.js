function log(message){
	window.status = "log: " + message;
}

function cmd(command, callback){
    if(callback === null)
        callback = ignore;
    window.status = "cmd: " + command + "|" + callback.toString();
}

function ignore(val){}

function pimove(x,y,angle){
    cmd("movepi.py "+x + " " +y + " " +angle, null);
}

function pimoveall(x,y,angle){
    for(var i = 0; i <= x; i++)
        for(var j = 0; j <= y; j++)
            pimove(i, j, angle);
}

