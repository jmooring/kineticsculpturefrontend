
boolean rotatable = true;


class Sculpture{
    int xdim = 18;
    int nodeCount = 256;
    float wrap = 270, wrapt = wrap;
    float pinch = 0.2, pincht = pinch;
    boolean spiral = true;
    int selected = -1;
    float pushDown = 25;
    float radius = 50, radiust = radius;
    float rotation = -0.19, rotationt = rotation, rotationMax = 4*PI;
    float[] extrusion = new float[256];

    float locZ = -135, locZt = locZ;

    Sculpture(){}

    void transition(){
      if(wrapt != 20){
        rotatable = false;
        rotationt = rotation + (2*PI % rotation) - radians(10);
        spiral = false;
        wrapt = 20;
        radiust = 550;
        locZt = -650;
        pincht = 0;
      }
      else{
        spiral = true;
        rotatable = true;
        wrapt = 270;
        radiust = 50;
        locZt = -135;
        pincht=0.2;
      }
    }

    void draw(){
      pushMatrix();

    //[0-100] coordinate system
    translate(width/2,height/2);
    scale(min(width,height)/100.0);
    translate(-50,-50);

    fill(200);
    pushMatrix();
    translate(50,100+pushDown*2,0);
    //box(300,5,700);
    popMatrix();

    float nearest = 1000;
    selected = -1;

    float degree = 0;
    float h = 100;

    for(int i = 0; i < nodeCount; i++){
      pushMatrix();
      translate(50, pushDown, locZ);
      rotateY(rotation);
      float r = radius + pinch*pow(i/xdim - (nodeCount/xdim)/2, 2);
      r += extrusion[i]*4;
      translate(-5,-5,-1);
      rotateY(radians(degree));
      translate(0,h,r);
      if(spiral)
          rotate(-atan2(1.0/xdim,2));
      fill(120-extrusion[i]*120,120-extrusion[i]*30,120+extrusion[i]*134);

      if(mousePressed && dist(mouseX,mouseY,screenX(0,0,1),screenY(0,0,1)) < 35)
        if(screenZ(0,0,1) < nearest){
          nearest = screenZ(5,5,1);
          selected = i;
        }

      box(10,10,2);

      degree += wrap/xdim;
      if(spiral)
      h -= 11.0/xdim;
      else if(degree >= wrap - 1)
      h -= 11.0;
      if(degree >= wrap - 1)
        degree = 0;

      extrusion[i] -= 0.005;

      if(extrusion[i] < 0)
        extrusion[i] = 0;
      if(extrusion[i] > 1)
        extrusion[i] = 1;

      if(extrusion[i] != 0 && frameCount % 10 == 0)
         window.cmd("pi move " + i%5 + " " + int(i/6) + " " + int(extrusion[i]*100),null);

      popMatrix();
    }

    if(selected >= 0)
      extrusion[selected]+=0.1;

    popMatrix();

    rotation = (rotation*15 + rotationt)/16;

    radius = (radius*10 + radiust)/11;
    wrap = (wrap * 10 + wrapt)/11;
    locZ = (locZ * 10 + locZt)/11;
    pinch = (pinch * 15 + pincht)/16;
    }


}

Sculpture sculpture;

void setup(){
  size(750,750,P3D);
  hint(ENABLE_OPENGL_2X_SMOOTH);
  noStroke();
  frameRate(30);
  background(0);
  sculpture = new Sculpture();

}

void draw(){

  background(0);

  directionalLight(255,255,255,-.5,-1,-.5);
  pointLight(255,255,255,0,-1000,0);

  sculpture.draw();



  if(rotatable){
    lights();
    fill(100);
    rect(width/6, 29.5*height/32, 4*width/6, height/32);
    fill(150);
    translate(0,0,1);
    rect(width/6 + ((width/1.5)-width/40)*sculpture.rotationt/sculpture.rotationMax, 29*height/32, width/40, 2*height/32);

    if(mousePressed && mouseX > width/6 && mouseX < 5*width/6 && mouseY > 28*height/32 && mouseY < 31*height/32)
        sculpture.rotationt = sculpture.rotationMax * (mouseX - width/6)/(4*width/6);

  }

}

void mousePressed(){
    if(mouseX < 50)
    sculpture.transition();
}
