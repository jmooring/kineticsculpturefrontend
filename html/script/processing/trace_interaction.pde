int xdim = 16;
int ydim = 16;

float[][] extrusion;
float alpha = 2, talpha = alpha;
float beta = 3, tbeta = beta;

int bwidth = 750;
int bheight = 750;
int tcount = 0;
float transfer = 0.001;

JavaScript javascript = null;

void setJS(JavaScript js){
  javascript = js;
}

void setup(){
    size(bwidth + 50, bheight);
    frameRate(30);
    extrusion = new float[xdim][ydim];
    colorMode(HSB,100);
}

void draw(){
    background(0);
    float sum = 0;
    float onCells = 0;
    for(int i = 0; i < xdim; i++){
        for(int j = 0; j < ydim; j++){
            float left = i*bwidth/xdim;
            float right = left + bwidth/xdim;
            float top = j*bheight/ydim;
            float bottom = top + bheight/ydim;
            if(mousePressed && mouseX > left && mouseX < right && mouseY > top && mouseY < bottom){
                extrusion[i][j] += 0.08;
                if(i < xdim-1)
                  extrusion[i+1][j] += 0.04;
                if(i > 0)
                  extrusion[i-1][j] += 0.04;
                if(j < ydim-1)
                  extrusion[i][j+1] += 0.04;
                if(j > 0)
                  extrusion[i][j-1] += 0.04;
            }
            if(extrusion[i][j] > 1)
              extrusion[i][j] = 1;
            if(extrusion[i][j] < 0)
              extrusion[i][j] = 0;

            float min = 10000;
            for(float t = alpha; t < alpha+2; t+=0.02){
              float x = 2+(xdim-4)*(cos(alpha*t)+1)/2;
              float y = 2+(ydim-4)*(sin(beta*t)+1)/2;
              float d = dist(i,j,x,y);
              if(d < min)
                min = d;
            }
            float val = pow(2.17,-min*min);

            if(val > 0.5){
              onCells++;
              sum += extrusion[i][j];
            }

            fill(8+extrusion[i][j]*8 + val*14, 100, 40 + extrusion[i][j]*50 + val*10);
            rect(i*bwidth/xdim ,j*bheight/ydim, bwidth/xdim, bheight/ydim,4);
            extrusion[i][j] -= 0.004;
            float local = extrusion[i][j]*transfer;
            if(i > 0)
              extrusion[i-1][j] += local;
            if(i < xdim-1)
              extrusion[i+1][j] += local;
            if(j > 0)
              extrusion[i][j-1] += local;
            if(j < ydim-1)
              extrusion[i][j+1] += local;
            if(javascript != null && frameCount % (i*xdim + j)/2 == 0)
                ;//javascript.window.cmd("pi move " + i + " " + j + " " + int(extrusion[i][j]*100), javascript.ignore);
        }
    }
    float prop = sum/onCells;
    float thresh = 0.288;
    if(prop > thresh){
      talpha = random(-3,3);
      tbeta = random(-3,3);
      for(int i = 0; i < xdim; i++)
        for(int j = 0; j < ydim; j++)
          extrusion[i][j] = 0;
      tcount++;
      if(tcount > 4)
        transfer=0.004;

    }
    alpha = (talpha + alpha*50)/51;
    beta = (tbeta + beta*50)/51;

    fill(32*prop/thresh, 100, 20+80*prop/thresh);
    rect(bwidth,height - height*prop/thresh, 50, height);

}
