int xdim = 6;
int ydim = 5;

float[][][] extrusion;

JavaScript javascript = null;

void setJS(JavaScript js){
  javascript = js;
}

void setup(){
    size(900*.7, 750*.7);
    frameRate(30);
    extrusion = new float[xdim][ydim][2];
    colorMode(HSB,100);
}

void draw(){
    background(0);
    for(int i = 0; i < xdim; i++){
        for(int j = 0; j < ydim; j++){
            float left = i*width/xdim;
            float right = left + width/xdim;
            float top = j*height/ydim;
            float bottom = top + height/ydim;
            if(mousePressed && mouseX > left && mouseX < right && mouseY > top && mouseY < bottom){
                if(extrusion[i][j][0] == 0)
                    window.cmd("pi move " + j + " " + i + " " + 100, null);
                extrusion[i][j][0] = 1;
            }
            else{
                if(extrusion[i][j][0] == 1)
                    window.cmd("pi move " + j + " " + i + " " + 0, null);
                extrusion[i][j][0] = 0;
            }
            
            float interp = 5;
            
            extrusion[i][j][1] = (extrusion[i][j][1]*(interp - 1) + extrusion[i][j][0])/interp;
            
            fill(8+extrusion[i][j][1]*8, 100, 40 + extrusion[i][j][1]*60);
            
            rect(i*width/xdim ,j*height/ydim, width/xdim, height/ydim,10);
            
                
        }
    }
}
