#!/bin/bash

${BASH_SOURCE%/*}/moveall.sh 80 > /dev/null
sleep 2
${BASH_SOURCE%/*}/moveall.sh 50 > /dev/null
sleep 1
${BASH_SOURCE%/*}/moveall.sh 80 > /dev/null
sleep 2

grep -i "$*" ${BASH_SOURCE%/*}/trees | awk -F ',' '{print $19}' | sort | uniq -c | sort -nr | head -n 15 | sed 's/[^ ]\+/\L\u&/g' | sed 's/[[:space:]]\{1,\}//' | sed 's/[[:space:]]\{1,\}/   /'

${BASH_SOURCE%/*}/moveall.sh 20 > /dev/null
