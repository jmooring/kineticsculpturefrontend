/** abstract paramters that can come from different sources (twitter, default, facebook...) **/

var parameterManager = {
    // queue that will ALWAYS have item. usually the dev provided list in settings
    defaultQueue : seemore_settings.defaultParameters, 

    // queueu which will contain tweet search parameters
    tweetQueue : [],

    /** return the next parameter in the queue **/
    pop : function() {
        // first see if there are any tweets to  render
        var param = seemoreAws.getKeyword();
        if (param) {
            // alert(JSON.stringify(param))
            return param;
        }

        // behavior for defalut queue (for now)
        param = this.defaultQueue.shift();
        this.defaultQueue.push(param);
        return param;
    },

    getDefault : function() {
        // make a defensive copy
        return this.defaultQueue.slice(0);
    }
};
