seemore_settings = {
    searchingForText : 'Searching for trees around {0}',
    defaultParameters : []
}

// global objects
function searchParameter (keyword, meta) {
    this.keyword = keyword;
    // for now, meta is just an image
    this.meta = meta;
};


// Add default search paramters here
seemore_settings.defaultParameters.push(new searchParameter('Arbutus Ridge', 'images/hoods/ArbutusRidge.png'))
seemore_settings.defaultParameters.push(new searchParameter('Downtown', 'images/hoods/Downtown.png'))
seemore_settings.defaultParameters.push(new searchParameter('Dunbar - Southlands', 'images/hoods/DunbarSouthlands.png'))
seemore_settings.defaultParameters.push(new searchParameter('Fairview', 'images/hoods/Fairview.png'))
seemore_settings.defaultParameters.push(new searchParameter('Grandview - Woodland', 'images/hoods/GrandviewWoodland.png'))
seemore_settings.defaultParameters.push(new searchParameter('Hastings - Sunrise', 'images/hoods/HastingsSunrise.png'))
seemore_settings.defaultParameters.push(new searchParameter('Killarney', 'images/hoods/Killarney.png'))
seemore_settings.defaultParameters.push(new searchParameter('Marpole', 'images/hoods/Marpole.png'))
seemore_settings.defaultParameters.push(new searchParameter('Mount Pleasant', 'images/hoods/MountPleasant.png'))
seemore_settings.defaultParameters.push(new searchParameter('Oakridge', 'images/hoods/Oakridge.png'))
seemore_settings.defaultParameters.push(new searchParameter('Riley Park', 'images/hoods/RileyPark.png'))
seemore_settings.defaultParameters.push(new searchParameter('South Cambie', 'images/hoods/SouthCambie.png'))
seemore_settings.defaultParameters.push(new searchParameter('Strathcona', 'images/hoods/Strathcona.png'))
seemore_settings.defaultParameters.push(new searchParameter('Sunset', 'images/hoods/Sunset.png'))
seemore_settings.defaultParameters.push(new searchParameter('Victoria - Fraserview', 'images/hoods/VictoriaFraserview.png'))
seemore_settings.defaultParameters.push(new searchParameter('West End', 'images/hoods/WestEnd.png'))
seemore_settings.defaultParameters.push(new searchParameter('West Point Grey', 'images/hoods/WestPointGrey.png'))

