#include "main.h"
#include "sculpturewidget.h"
#include <QPushButton>
#include <QUdpSocket>
#include <QtConcurrent/QtConcurrent>

#include <math.h>
#include <stdio.h>

using namespace cprocessing;


QUdpSocket socket;

#define TPOINTS 32

int touchPoints[TPOINTS][2] = {{-1}};
int selected[TPOINTS][2] = {{-1}};


SculptureWidget::SculptureWidget(QWidget *parent, QWebView* webView)
    : QGLWidget(QGLFormat(QGL::SampleBuffers), parent)
{
    this->setAttribute(Qt::WA_AcceptTouchEvents,true);
    makeCurrent();
    glClearColor(0,0,0,1);
    glClear(GL_COLOR_BUFFER_BIT);

    animationTimer.setSingleShot(false);
    connect(&animationTimer, SIGNAL(timeout()), this, SLOT(animate()));
    animationTimer.start(17);
    setAutoFillBackground(false);
    grabKeyboard();
    this->webView = webView;
    socket.bind(QHostAddress("192.168.1.100"), 10001);
    qDebug() << socket.errorString() ;
    QObject::connect(&socket,&QUdpSocket::bytesWritten,[](qint64 written){qDebug()<<written;});

    for(int i = 0;i < TPOINTS; i++){
        touchPoints[i][0] = -1;
        touchPoints[i][1] = -1;
    }
}


void renderSculpture(float shearX, float shearZ, float shadow);




//4x6
// shadow on wall behind the sculpture
int nodeCount = 30;
float wrap = 10;
bool spiral = false;
float radius = 440;
float pushDown = -25;
float rotation = .1, rotationt = rotation, rotationMax = .2, rotationOffset = -.2;
float pinch = 0.0;
float xdim = 6;
float locZ = -435;
float extrusion[256][2];

//int nodeCount = 256;
//float wrap = 270;
//bool spiral = true;
//float radius = 50;
//float pushDown = 25;
//float rotation = 0, rotationt = rotation, rotationMax = 2*PI, rotationOffset = 0;
//float pinch = 0.2; //hourglass factor
//float xdim = 18;
//float locZ = -135;
//float extrusion[256][2];

bool buttons = true;
bool shadows = true;
bool showFloor = true;

int mapping[] = {6,5,4,3,2,1,12,11,10,9,8,7,18,17,16,15,14,13,24,23,22,21,20,19,30,29,28,27,26,25};


//cprocessing looks for ::draw
void draw(){
    background(0);

    for(int i = 0; i < TPOINTS; i++){
        selected[i][0] = -1;
    }

    directionalLight(255,255,255,.5,1,-.25);
    pointLight(180,180,180,30,2000,1000);

         // 5x6 sculpture
        //main sculpture
    pushMatrix();
    scale(1.2,1,1);
    renderSculpture(0,0,-1);
    popMatrix();


    if(showFloor){ //floor
        pushMatrix();
        translate(width/2,height*1.35,-500);
        fill(100);
        pointLight(180,180,180,0,100,200);
        box(4000,4000,7);
        popMatrix();
    }
    if(showFloor && shadows){  //shadow
        pushMatrix();
        translate(0,0,-40);
        scale(1.05,1.05,0.0001);
        noStroke();
        noLights();
        pushMatrix();

        scale(1.2,1,1);
        renderSculpture(0,0,10);
        popMatrix();
        popMatrix();
    }




    //    //main sculpture
    //    renderSculpture(0,0,-1);



    //    if(showFloor){ //floor
    //        pushMatrix();
    //        translate(width/2,height*1.35,0);
    //        fill(80);
    //        pointLight(180,180,180,0,100,4000);
    //        box(4000,10,7000);
    //        popMatrix();
    //    }
    //    if(showFloor && shadows){  //shadow
    //        pushMatrix();
    //        translate(0,height*1.3,0);
    //        scale(1.1,0.0001,1.1);
    //        noStroke();
    //        noLights();
    //        renderSculpture(0.3,-0.3,0.1);
    //        popMatrix();
    //    }

    //rotation bar
//        float barWidth = 600;
//        float sliderWidth = 20;
//        {
//            pushMatrix();
//            noLights();
//            fill(100);
//            rect(width/2 - barWidth/2, 29.5*height/32, barWidth, height/32);
//            fill(150);
//            translate(0,0,1);
//            rect(width/2 - barWidth/2 + (barWidth-sliderWidth)*rotationt/rotationMax, 29*height/32, sliderWidth, 2*height/32);
//            for(int i = 0; i < TPOINTS; i++){
//                if(touchPoints[i][0] >= 0 && touchPoints[i][0] > width/2 - barWidth/2 && touchPoints[i][0] < width/2+barWidth/2 &&
//                        touchPoints[i][1] > 28*height/32 && touchPoints[i][1] < 31*height/32)
//                    rotationt = rotationMax * (touchPoints[i][0] - (width/2-barWidth/2))/(barWidth);
//            }
//            popMatrix();
//        }


}


void renderSculpture(float shearX, float shearZ, float shadow){
    pushMatrix();

    //scale to [0,100] coordinate system
    translate(width/2,height/2);
    scale(min(width,height)/100.0);
    translate(-50,-50);

    float nearest[TPOINTS];
    for(int j = 0; j < TPOINTS; j++){
        nearest[j] = 1000;
    }

    float degree = 0;
    float h = 100;

    float x = shearX; float z = shearZ; //shadow shearing

    for(int i = 0; i < nodeCount; i++){
        pushMatrix();
        translate(50, pushDown, locZ);
        translate(x,0,z);
        rotateY(rotationOffset + rotation);
        translate(-5,-5,-1);
        float r = radius + pinch*pow(i/xdim - (nodeCount/xdim)/2, 2); // hourglass form
        r += extrusion[i][0]*4;
        rotateY(radians(degree));
        translate(0,h,r);

        if(shadow > 0 && nodeCount < 35)
            translate(extrusion[i][0],extrusion[i][0],0);
        if(spiral)
            rotate(-atan2(1.0/xdim,2));
        fill(120-extrusion[i][0]*120,120-extrusion[i][0]*30,120+extrusion[i][0]*134);
        if(shadow>0)
            fill(shadow);

        //selection
        for(int j = 0; j < TPOINTS; j++){
            if(shadow<0 && touchPoints[j][0] >= 0  && dist(touchPoints[j][0],touchPoints[j][1],screenX(0,0,1),height-screenY(0,0,1)) < 85){
                if(screenZ(0,0,1) < nearest[j]){
                    nearest[j] = screenZ(5,5,1);
                    selected[j][0] = i;
                }
            }
        }

        extrusion[i][0] = (extrusion[i][0]*7+extrusion[i][1])/8; //interpolate to target extrusion
        extrusion[i][1] = 0;

        if(shadow > 0){
            noStroke();
            rotateX(i/2560.);
            scale(1+i/2560.);
        }else{
            stroke(30);
        }

        box(10,10,2);

        //elevation
        degree += wrap/xdim;
        if(spiral)
            h -= 11.0/xdim;
        else if(degree >= wrap - 1){
            h -= 11.0;
        }

        if(degree >= wrap - 1){
            degree = 0;
            x += 2*shearX;
            z += 2*shearZ;
        }

        popMatrix();
    }

    for(int i = 0; i < TPOINTS; i++){
        if(selected[i][0] >= 0){
            if(selected[i][0] != selected[i][1]){
                qDebug() << QHostAddress("192.168.1."+QString::number(selected[i][0]+1));
                if(selected[i][0] >= 0){
                    qDebug() << selected[i][0] << " " << 100;
                    socket.writeDatagram(QString("moveservo 100").toLocal8Bit(),QHostAddress("192.168.1."+QString::number(mapping[selected[i][0]])),3000);
                }
                if(selected[i][1] >= 0){
                    qDebug() << selected[i][1] << " " << 0;
                    socket.writeDatagram(QString("moveservo 0").toLocal8Bit(),QHostAddress("192.168.1."+QString::number(mapping[selected[i][1]])),3000);
                }
                socket.flush();
            }
            extrusion[selected[i][0]][1] = 1;
        }else{
            if(selected[i][0] != selected[i][1]){
                socket.writeDatagram(QString("moveservo 0").toLocal8Bit(),QHostAddress("192.168.1."+QString::number(mapping[selected[i][1]])),3000);
            }
        }
        selected[i][1] = selected[i][0];
    }


    rotation = (rotation*15 + rotationt)/16; //interpolate to target rotation

    popMatrix();

}

bool SculptureWidget::event(QEvent * e){
    bool ret = QGLWidget::event(e);

    QInputEvent* input = dynamic_cast<QInputEvent*>(e);
    if(input)
    {
        input->accept();
        QTouchEvent *touchEvent = static_cast<QTouchEvent *>(e);

        int i = 0;
        for(QTouchEvent::TouchPoint point : touchEvent->touchPoints()){
            if(point.state() == Qt::TouchPointReleased){
                touchPoints[i][0] = -1;
                touchPoints[i][1] = -1;
            }else{
            touchPoints[i][0] = (int)point.pos().x();
            touchPoints[i][1] = (int)point.pos().y();
            }
            i++;
        }
        for(;i<TPOINTS;i++){
            touchPoints[i][0] = -1;
            touchPoints[i][1] = -1;
        }
    }

    return ret;
}


void SculptureWidget::resizeGL(int w, int h)
{
    cprocessing::reshape(w,h);
}


QFont serifFont("Helvetica Neue", 28);
QFontMetrics fm(serifFont);
void SculptureWidget::paintEvent(QPaintEvent *event)
{
    if(event == 0)
        return;
    makeCurrent();
    QPainter p(this);
    p.beginNativePainting();
    glEnable(GL_CULL_FACE);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_MULTISAMPLE);
    cprocessing::smooth();
    cprocessing::display();
    p.endNativePainting();


    //overpainting setup
    glShadeModel(GL_FLAT);
    glDisable(GL_CULL_FACE);
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_LIGHTING);
    glMatrixMode(GL_MODELVIEW);


    //overpainting example
    if(buttons && shadows && showFloor){
        p.setRenderHint(QPainter::Antialiasing);
        p.setPen(QPen(QBrush(QColor(255,255,255)),1));
        p.setBrush(QBrush(QColor(90,90,90)));
        p.setFont(serifFont);
        int padding = 50;
        {
            QString s("Back");
            int w = fm.width(s) + padding;
            int h = fm.height() + padding;
            QRectF rect(width()-w-padding,height()-h-padding,w,h);
            for(int i = 0; i < TPOINTS; i++){
                if(touchPoints[i][0] >= 0 && rect.contains(touchPoints[i][0],touchPoints[i][1])){
                    p.setBrush(QBrush(QColor(35,35,35)));
                    webView->load(QUrl::fromLocalFile(QFileInfo("html/index.html").absoluteFilePath()));
                }
            }
            p.drawRoundedRect(rect,4,4);
            p.drawText(rect,Qt::AlignCenter,s);
        }
    }

    p.end();
}


void SculptureWidget::initializeGL()
{
    glClearColor(0,0,0,1);
    glClear(GL_COLOR_BUFFER_BIT);
    cprocessing::size(400,400,"");
    mousePressed = false;

}

SculptureWidget::~SculptureWidget()
{
    socket.close();
    releaseMouse();
    animationTimer.stop();
}

