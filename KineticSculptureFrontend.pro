#-------------------------------------------------
#
# Project created by QtCreator 2014-02-05T21:54:40
#
#-------------------------------------------------

QT       += core webkitwidgets opengl gui network multimedia multimediawidgets

QMAKE_CXXFLAGS += -std=c++0x

LIBS+=-g -lglut -lGLU -lGL -lGLEW -lglfw -lfreeimage

greaterThan(QT_MAJOR_VERSION, 5): QT += widgets

TARGET = KineticSculptureFrontend
TEMPLATE = app

SOURCES += \
    cprocessing/transformations.cpp \
    cprocessing/style.cpp \
    cprocessing/string.cpp \
    cprocessing/snoise.cpp \
    cprocessing/shapes.cpp \
    cprocessing/screenpixels.cpp \
    cprocessing/ptime.cpp \
    cprocessing/pthread.cpp \
    cprocessing/pshader.cpp \
    cprocessing/primitives.cpp \
    cprocessing/pnoise.cpp \
    cprocessing/pixelcolorbuffer.cpp \
    cprocessing/pimage.cpp \
    cprocessing/pcamera.cpp \
    cprocessing/lights.cpp \
    cprocessing/io.cpp \
    cprocessing/extfuncs.cpp \
    cprocessing/cprocessing.cpp \
    cprocessing/color.cpp \
    cprocessing/attributes.cpp \
    main.cpp \
    pluginfactory.cpp \
    sculpturewidget.cpp

HEADERS  += \
    cprocessing/transformations.hpp \
    cprocessing/style.hpp \
    cprocessing/string.hpp \
    cprocessing/snoise.hpp \
    cprocessing/shapes.hpp \
    cprocessing/screenpixels.hpp \
    cprocessing/random.hpp \
    cprocessing/pvector.hpp \
    cprocessing/ptime.hpp \
    cprocessing/pthread.hpp \
    cprocessing/pshader.hpp \
    cprocessing/primitives.hpp \
    cprocessing/pnoise.hpp \
    cprocessing/pixelcolorbuffer.hpp \
    cprocessing/pimage.hpp \
    cprocessing/pconstant.hpp \
    cprocessing/pcamera.hpp \
    cprocessing/maths.hpp \
    cprocessing/lights.hpp \
    cprocessing/io.hpp \
    cprocessing/init.hpp \
    cprocessing/hashmap.hpp \
    cprocessing/cprocessing.hpp \
    cprocessing/color.hpp \
    cprocessing/attributes.hpp \
    cprocessing/arraylist.hpp \
    main.h \
    pluginfactory.h \
    sculpturewidget.h

FORMS    +=

OTHER_FILES += \
    html/index.html \
    html/styles/master.css \
    html/parallelism.html \
    html/making_of.html \
    html/get_to_know.html \
    html/applications.html \
    html/parallelism_story.html \
    html/swipe_interaction.html \
    html/script/processing/swipe_interaction.pde \
    html/script/processing/trace_interaction.pde \
    html/trace_interaction.html \
    html/get_to_know_story.html \
    html/making_of_story.html \
    html/script/sculpture_interface.js \
    html/script/processing/swipe2D.pde \
    html/swipe2D.html \
    html/embedded_gl_test.html \
    html/movie.html \
    html/quiz/quiz.html \
    html/start.html \
    html/mapreduce/mapreduce1.html \
    html/mapreduce/mapreduce4.html \
    html/mapreduce/mapreduce3.html \
    html/mapreduce/mapreduce2.html \
    html/mapreduce/mapreduce8.html \
    html/mapreduce/mapreduce7.html \
    html/mapreduce/mapreduce6.html \
    html/mapreduce/mapreduce5.html \
    html/map_search.html
