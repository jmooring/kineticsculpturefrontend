#ifndef SCULPTUREWIDGET_H
#define SCULPTUREWIDGET_H
#include <QtOpenGL>
#include <QtWebKitWidgets/QWebView>
#include <QUdpSocket>

void draw();

class SculptureWidget : public QGLWidget
{
    Q_OBJECT
public:
    SculptureWidget(QWidget *parent = 0, QWebView* webView = 0);
    ~SculptureWidget();
    void initializeGL();
    void resizeGL(int w, int h);
    void timerEvent(QTimerEvent *) { update(); }


    void paintEvent(QPaintEvent *event);

    bool event(QEvent *);




private:
    QTimer animationTimer;
    QWebView* webView;

private slots:
    void animate(){update();}
};
#endif // SCULPTUREWIDGET_H
