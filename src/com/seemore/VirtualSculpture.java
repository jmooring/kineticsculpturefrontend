package com.seemore;

import processing.core.PApplet;
import processing.core.PGraphics;

/**
 * Created by John on 9/2/2015.
 */
public class VirtualSculpture extends PApplet {
    int TPOINTS = 1;
    int[][] selected = new int[TPOINTS][2];

    public void setup() {
        size(1920, 1080, P3D);
        background(0);
        strokeWeight(0.05f);
        smooth(8);
    }

    int nodeCount = 256;
    float wrap = 270;
    float radius = 50;
    float pushDown = 25;
    float rotation = 0, rotationt = rotation, rotationMax = 2 * PI, rotationOffset = 0;
    float pinch = 0.2f; //hourglass factor
    int xdim = 18;
    float locZ = -135;
    float extrusion[][] = new float[256][2];
    int t0 = millis();
    int ts = 0;

    public void draw() {
        background(255);
        ts = millis() - t0;
        t0 = millis();

        for (int i = 0; i < TPOINTS; i++) {
            selected[i][0] = -1;
        }

        ambientLight(60, 60, 60);
        directionalLight(200, 200, 200, 0, 0, -.9f);
        directionalLight(255, 255, 255, 0, 1, .25f);

        //main sculpture
        renderSculpture(0, 0, -1);

        //shadow
        pushMatrix();
        translate(0, height * 1.3f, 0);
        scale(1, 0, 1);
        noStroke();
        noLights();
        renderSculpture(0.3f, -0.3f, 160);
        popMatrix();
    }

    int getX(int node) {
        return node % xdim;
    }

    int getY(int node) {
        return node / xdim;
    }

    boolean sweeping = false;
    int tstart;
//float sweep(int node){
//  if(!sweeping){
//    sweeping = true;
//    tstart = millis();
//  }
//  float time = (millis() - tstart)/1000.0 + 256;

//  if(abs((time*5 - node) % 19) < 6){
//    return 1;
//  }

//  return 0;
//}

//float sweep(int node){
//  if(!sweeping){
//    sweeping = true;
//    tstart = millis();
//  }
//  float time = (millis() - tstart)/1000.0 + 256;

//  float x = getX(node) - 8.5;
//  float y = getY(node) - 6.5;

//  return cos(sqrt(x*x+y*y)-time*2)>0.7?1:0;
//}

//    float sweep(int node){
//        if(!sweeping){
//            sweeping = true;
//            tstart = millis();
//        }
//        float time = (millis() - tstart)/1000f;
//
//        float x = getX(node) - 8.5f;
//        float y = getY(node) - 6.5f;
//
//        return abs(x*x - y + 64 - time*5) < 24 ? 1 : 0;
//    }

    boolean isPrime(int n) {
        if (n % 2 == 0) return false;
        for (int i = 3; i * i <= n; i += 2) {
            if (n % i == 0)
                return false;
        }
        return true;
    }

    int ulam(int x, int y) {
        int r = max(abs(x), abs(y));
        if (x + y <= 0)
            return 1 + 4 * r * r + 2 * r + x - y;
        return 1 + 4 * r * r - 2 * r - x + y;
    }

    float sweep(int node) {
        if (!sweeping) {
            sweeping = true;
            tstart = millis();
        }

        float time = (millis() - tstart) / 1000.0f - 2;

        int x = getX(node) - 8;
        int y = getY(node) - 6;
        int u = ulam(x, y);
        float t = time * (time * 0.1f) * 10;
        if (t < 324)
            return u < t ? 1 : 0;
        if (t < 648)
            return u < t - 324 && !isPrime(u) ? 0 : 1;
        return isPrime(u) && (x - 8) * 4 + (y - 8) * 4 < 660 - t ? 1 : 0;
    }

    void renderSculpture(float shearX, float shearZ, float shadow) {
        pushMatrix();

        //scale to [0,100] coordinate system
        translate(width / 2, height / 2);
        scale(min(width, height) / 100.0f);
        translate(-50, -50);

        float[] nearest = new float[TPOINTS];
        for (int j = 0; j < TPOINTS; j++) {
            nearest[j] = 1000;
        }

        float degree = 0;
        float h = 100;

        float x = shearX;
        float z = shearZ; //shadow shearing

        for (int i = 0; i < nodeCount; i++) {
            pushMatrix();
            translate(50, pushDown, locZ);
            translate(x, 0, z);
            rotateY(rotationOffset + rotation);
            translate(-5, -5, -1);
            float r = radius + pinch * pow(i / xdim - (nodeCount / xdim) / 2, 2); // hourglass form
            r += extrusion[i][0] * 4;
            rotateY(radians(degree));
            translate(0, h, r);

            rotate(-atan2(1.0f / xdim, 2));
            fill(220 - extrusion[i][0] * 120, 220 + extrusion[i][0] * 134, 220 - extrusion[i][0] * 30);

            if (shadow > 0)
                fill(shadow);

            if (shadow < 0 && dist(mouseX, mouseY, screenX(0, 0, 1), screenY(0, 0, 1)) < 45) {
                if (screenZ(0, 0, 1) < nearest[0]) {
                    nearest[0] = screenZ(4, 4, 1);
                    selected[0][0] = i;
                }
            }

            extrusion[i][0] = (extrusion[i][0] * 7 + extrusion[i][1]) / 8; //interpolate to target extrusion
            extrusion[i][1] = sweep(i);

            if (shadow > 0) {
                noStroke();
                rotateX(i / 2560f);
                scale(1 + i / 2560f);
            } else {
                stroke(30);
            }

            box(10, 10, 2);

            //elevation
            degree += wrap / xdim;
            h -= 11.0 / xdim;

            if (degree >= wrap - 1) {
                degree = 0;
                x += 2 * shearX;
                z += 2 * shearZ;
            }

            popMatrix();
        }

        for (int i = 0; i < TPOINTS; i++) {
            if (selected[i][0] >= 0) {
                if (selected[i][0] != selected[i][1]) {
                    // qDebug() << QHostAddress("192.168.1."+QString::number(selected[i][0]+1));
                    if (selected[i][0] >= 0) {
                        //    qDebug() << selected[i][0] << " " << 100;
                        //    socket.writeDatagram(QString("moveservo 100").toLocal8Bit(),QHostAddress("192.168.1."+QString::number(mapping[selected[i][0]])),3000);
                    }
                    if (selected[i][1] >= 0) {
                        //    qDebug() << selected[i][1] << " " << 0;
                        //    socket.writeDatagram(QString("moveservo 0").toLocal8Bit(),QHostAddress("192.168.1."+QString::number(mapping[selected[i][1]])),3000);
                    }
                    // socket.flush();
                }
                extrusion[selected[i][0]][1] = 1;
            } else {
                if (selected[i][0] != selected[i][1]) {
                    //  socket.writeDatagram(QString("moveservo 0").toLocal8Bit(),QHostAddress("192.168.1."+QString::number(mapping[selected[i][1]])),3000);
                }
            }
            selected[i][1] = selected[i][0];
        }


        //rotation = (rotation*15 + rotationt)/16; //interpolate to target rotation
        rotation += ts * 0.00005;
        if (mousePressed) {
            rotation += (mouseX - pmouseX) * 0.001;
        }
        popMatrix();

    }
}
