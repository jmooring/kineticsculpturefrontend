package com.seemore;

import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebEvent;
import javafx.scene.web.WebView;
import org.apache.commons.io.IOUtils;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

public class Main {

    private static WebEngine webEngine;

    private static boolean sculptureVisible = false;

    public static void main(String[] args) throws InterruptedException {

        JFrame gui = new JFrame();
        VirtualSculpture vs = new VirtualSculpture();
        gui.getContentPane().setLayout(null);
        gui.getContentPane().add(vs);


        JFXPanel jfxPanel = new JFXPanel();
        Platform.runLater(() -> {
            WebView browser = new WebView();
            webEngine = browser.getEngine();
            webEngine.load(Main.class.getResource("/map_search.html").toExternalForm());
            webEngine.setOnStatusChanged(new ScriptInterface());
            jfxPanel.setScene(new Scene(browser));
        });

        gui.getContentPane().add(jfxPanel);

        jfxPanel.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                sculptureVisible = !sculptureVisible;
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

        gui.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        jfxPanel.setBounds(gui.getWidth(), 0, gui.getWidth(), gui.getHeight());
        gui.setUndecorated(true);

        gui.setVisible(true);

        gui.setSize(Toolkit.getDefaultToolkit().getScreenSize());

        vs.init();
        int w = gui.getContentPane().getWidth();
        int h = gui.getContentPane().getHeight();
        int x = w-10;
        while (true) {
            jfxPanel.setBounds(0, 0, w, h);
            if(sculptureVisible && x > 0){
                x = (int)(x * 0.7);
            }
            if(!sculptureVisible && x < w){
                x = (int)(x * 1.3) + 1;
            }
            vs.setBounds(x, 0, w, h);
            vs.resize(w,h);
            gui.invalidate();
            Thread.sleep(30);
        }
    }

    private static class ScriptInterface implements EventHandler<WebEvent<String>> {
        @Override
        public void handle(WebEvent<String> event) {
            if (event == null) {
                return;
            }

            String msg = event.getData();

            if (msg == null) {
                return;
            }

            if (msg.startsWith("log: ")) {
                msg = msg.substring(5);
                System.out.println("Logging: " + msg);
            }

            if (msg.startsWith("cmd: ")) {
                msg = msg.substring(5);
                String[] cmd = msg.split("\\|");
                System.out.println(msg);

                if (cmd.length != 2) {
                    System.out.println("EARLY EXIT " + cmd.length);
                    return;
                }

                String command = cmd[0];
                String callback = cmd[1];


                Runnable future = new FutureTask<String>(() -> {
                    String output = null;

                    try {
                        Process p = Runtime.getRuntime().exec("bash -c \"" + command + "\"");
                        InputStream procOut = p.getInputStream();
                        InputStream procErr = p.getErrorStream();
                        p.waitFor();
                        output = IOUtils.toString(procOut);
                        System.out.println(IOUtils.toString(procErr));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    System.out.println(output);
                    return output;
                }) {
                    @Override
                    protected void done() {

                        String output = null;
                        try {
                            output = this.get();
                            if (output == null) {
                                output = "No trees found.";
                            }
                            output = output.replaceAll("'", "");
                            output = output.replace("\n", "<br>");
                            output = output.replace("\r", "");
                            System.out.println(output);
                        } catch (InterruptedException | ExecutionException e) {
                            e.printStackTrace();
                        }
                        final String result = output;
                        Platform.runLater(() -> webEngine.executeScript("(" + callback + ")('" + result + "')"));

                        System.out.println("(" + callback + ")('" + output + "')");
                    }
                };
                new Thread(future).start();
            }
        }
    }

}