#include "main.h"
#include <QApplication>
#include <QtWidgets/qwidget.h>
#include <QtWebKit>
#include <QMainWindow>
#include <QtWebKitWidgets/QWebView>
#include <QtWebKitWidgets/QWebFrame>
#include <QProcess>
#include <QThread>
#include <QtConcurrent/QtConcurrent>
#include <QMetaType>
#include <QWebSettings>
#include <QtMultimediaWidgets/QVideoWidget>
#include <QMediaPlayer>
#include "pluginfactory.h"

void handleMessage(QString msg);
void jsCallback(int result);

QWebView* webView;

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    //a.setOverrideCursor(QCursor(Qt::BlankCursor)); //hide the cursor
    QMainWindow w;

    QWebSettings::globalSettings()->setAttribute( QWebSettings::PluginsEnabled, true );


    webView = new QWebView(&w);
    webView->setAttribute(Qt::WA_AcceptTouchEvents, true);

    PluginFactory *factory = new PluginFactory(webView);
    webView->page()->setPluginFactory(factory);

    webView->load(QUrl::fromLocalFile(QFileInfo("html/map_search.html").absoluteFilePath()));
    webView->show();

    QObject::connect(webView, &QWebView::statusBarMessage, handleMessage);

    w.setCentralWidget(webView);
    w.showFullScreen();

    return a.exec();
}

void handleMessage(QString msg){
    if(msg.startsWith("log: ")){
        msg = msg.mid(5);
        qDebug() << "Logging: " + msg;
    }

    if(msg.startsWith("cmd: ")){
        msg = msg.mid(5);
        qDebug() << msg;

        if(msg.split("|").length() != 2)
            return;

        QString command = msg.split("|").at(0);
        QString callback = msg.split("|").at(1);

        QFutureWatcher<QString>* watcher = new QFutureWatcher<QString>();

        QFuture<QString> future = QtConcurrent::run([=]{
            QProcess p;
            p.start("bash -c \"" + command + "\"");
            //p.start("dbus-launch bash -c \"source /home/bm/.bashrc; " + command + "\"");
            p.waitForFinished();
            QString output(p.readAll());
            qDebug() << output;
            return output;
        });

        QObject::connect(watcher,&QFutureWatcher<QString>::resultReadyAt,[=](int result){
            QString output = watcher->resultAt(result);
            output.remove(QRegExp("[']"));
            output.replace("\n","<br>");
            output.replace("\r","");
            qDebug() << output;
            webView->page()->mainFrame()->evaluateJavaScript("("+callback+")('"+output+"')");
            qDebug() << "("+callback+")('"+output+"')";
            delete watcher;
        });

        watcher->setFuture(future);
    }
}
